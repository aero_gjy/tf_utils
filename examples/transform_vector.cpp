/*
* Copyright (c) 2016 Carnegie Mellon University, Author <sanjiban@cmu.edu>
*
* For License information please see the LICENSE file in the root directory.
*
*/


/* Copyright 2014 Sanjiban Choudhury
 * transform_vector.cpp
 *
 *  Created on: May 26, 2014
 *      Author: Sanjiban Choudhury
 */

#include <ros/ros.h>
#include "tf_utils/tf_utils.h"
using namespace ca;
namespace tu = tf_utils;

int main(int argc, char **argv) {
  ros::init(argc, argv, "transform_vector");

  {
    Eigen::Vector3d vec(1,1,1);
    tf::Transform transform(tf::createIdentityQuaternion());
    transform.setOrigin(tf::Vector3(2,2,2)); // translate by 2, 2, 2
    tu::transformVector3d(transform, vec);
    ROS_INFO_STREAM("Transformed vector: "<<vec.transpose());
  }
  {
    Eigen::Vector3f vec(1,1,1);
    tf::Transform transform(tf::createIdentityQuaternion());
    transform.setOrigin(tf::Vector3(2,2,2)); // translate by 2, 2, 2
    tu::transformVector3f(transform, vec);
    ROS_INFO_STREAM("Transformed vector: "<<vec.transpose());
  }
}
